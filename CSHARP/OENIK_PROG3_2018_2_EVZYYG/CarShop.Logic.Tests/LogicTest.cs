﻿// <copyright file="LogicTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CarShop.Data;
    using CarShop.Logic;
    using CarShop.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// This class responsible for testing the Logic with mocked repository
    /// </summary>
    [TestFixture]
   public class LogicTest
    {
        private Logic l;
        private Mock<IRepository> mockedRepo;

        /// <summary>
        /// Here I setup the variables for further operations
        /// </summary>
        [SetUp]
        public void SetupLogic()
        {
            this.mockedRepo = new Mock<IRepository>();
            this.l = new Logic(this.mockedRepo.Object);
        }

        /// <summary>
        /// Test GetAllModel() method
        /// </summary>
        [Test]
        public void GetAllModelTest()
        {
            List<CARMODEL> mockModels = new List<CARMODEL>();
            CARMODEL x = new CARMODEL();
            CARMODEL y = new CARMODEL();
            mockModels.Add(x);
            mockModels.Add(y);

            this.mockedRepo.Setup(repo => repo.GetAllModels()).Returns(mockModels);
            Assert.That(this.l.GetAllModels() != null);
        }

        /// <summary>
        /// Test GetAllBrand() method
        /// </summary>
        [Test]
        public void GetAllBrandTest()
        {
            List<CARBRAND> mockBrands = new List<CARBRAND>();
            CARBRAND x = new CARBRAND();
            CARBRAND y = new CARBRAND();
            mockBrands.Add(x);
            mockBrands.Add(y);

            this.mockedRepo.Setup(repo => repo.GetAllBrands()).Returns(mockBrands);
            Assert.That(this.l.GetAllBrands() != null);
        }

        /// <summary>
        /// Test GetAllExtra() method
        /// </summary>
        [Test]
        public void GetAllExtrasTest()
        {
            List<EXTRA> mockExtras = new List<EXTRA>();
            EXTRA x = new EXTRA();
            EXTRA y = new EXTRA();
            mockExtras.Add(x);
            mockExtras.Add(y);

            this.mockedRepo.Setup(repo => repo.GetAllExtras()).Returns(mockExtras);
            Assert.That(this.l.GetAllExtras() != null);
        }

        /// <summary>
        /// Test GetAllLink() method
        /// </summary>
        [Test]
        public void GetAllLinksTest()
        {
            List<LINK_MOD_EXTRA> mockLinks = new List<LINK_MOD_EXTRA>();
            LINK_MOD_EXTRA x = new LINK_MOD_EXTRA();
            LINK_MOD_EXTRA y = new LINK_MOD_EXTRA();
            mockLinks.Add(x);
            mockLinks.Add(y);

            this.mockedRepo.Setup(repo => repo.GetAllLinks()).Returns(mockLinks);
            Assert.That(this.l.GetAllLinks() != null);
        }

        /// <summary>
        /// Test AddModel() method
        /// </summary>
        [Test]
        public void AddModelTest()
        {
            Mock<CARMODEL> mockModel = new Mock<CARMODEL>();
            this.l.AddModel(mockModel.Object);
        }

        /// <summary>
        /// Test AddBrand() method
        /// </summary>
        [Test]
        public void AddBrandTest()
        {
            Mock<CARBRAND> mockBrand = new Mock<CARBRAND>();
            this.l.AddBrand(mockBrand.Object);
        }

        /// <summary>
        /// Test AddExtra() method
        /// </summary>
        [Test]
        public void AddExtraTest()
        {
            Mock<EXTRA> mockExtra = new Mock<EXTRA>();
            this.l.AddExtra(mockExtra.Object);
        }

        /// <summary>
        /// Test AddLink() method
        /// </summary>
        [Test]
        public void AddLinkTest()
        {
            Mock<LINK_MOD_EXTRA> mockLink = new Mock<LINK_MOD_EXTRA>();
            this.l.AddLink(mockLink.Object);
        }

        /// <summary>
        /// Test modify model method
        /// </summary>
        [Test]
        public void ModifyModelNameTest()
        {
            List<CARMODEL> mockModels = new List<CARMODEL>();
            CARMODEL x = new CARMODEL();
            CARMODEL y = new CARMODEL();

            x.ID = "a";
            x.NAME = "kecske";

            mockModels.Add(x);
            mockModels.Add(y);

            this.mockedRepo.Setup(repo => repo.GetAllModels()).Returns(mockModels);

            CARMODEL tempNew = new CARMODEL();
            tempNew.ID = x.ID;
            tempNew.NAME = "macska";
            this.l.UpdateModel(tempNew);
        }

        /// <summary>
        /// Test modify brand method
        /// </summary>
        [Test]
        public void ModifyBrandTest()
        {
            Mock<CARBRAND> mockBrand = new Mock<CARBRAND>();
            CARBRAND tempNew = new CARBRAND();
            tempNew.ID = mockBrand.Object.ID;
            tempNew.COUNTRY = "Jamaika";
            this.l.UpdateBrand(tempNew);
        }

        /// <summary>
        /// Test modify extra method
        /// </summary>
        [Test]
        public void ModifyExtraTest()
        {
            Mock<EXTRA> mockExtra = new Mock<EXTRA>();
            EXTRA tempNew = new EXTRA();
            tempNew.ID = mockExtra.Object.ID;
            tempNew.REUSABLE = !mockExtra.Object.REUSABLE;
            this.l.UpdateExtra(tempNew);
        }

        /// <summary>
        /// Test remove model method
        /// </summary>
        [Test]
        public void RemoveModelTest()
        {
            Mock<CARMODEL> mockModel = new Mock<CARMODEL>();
            this.l.AddModel(mockModel.Object);
            this.l.DeleteModel(mockModel.Object);
        }

        /// <summary>
        /// Test remove brand method
        /// </summary>
        [Test]
        public void RemoveBrandTest()
        {
            Mock<CARBRAND> mockBrand = new Mock<CARBRAND>();
            this.l.AddBrand(mockBrand.Object);
            this.l.DeleteBrand(mockBrand.Object);
        }

        /// <summary>
        /// Test remove extra method
        /// </summary>
        [Test]
        public void RemoveExtraTest()
        {
            Mock<EXTRA> mockExtra = new Mock<EXTRA>();
            this.l.AddExtra(mockExtra.Object);
            this.l.DeleteExtra(mockExtra.Object);
        }

        /// <summary>
        /// Test remove link method
        /// </summary>
        [Test]
        public void RemoveLinkTest()
        {
            Mock<LINK_MOD_EXTRA> mockLink = new Mock<LINK_MOD_EXTRA>();
            this.l.AddLink(mockLink.Object);
            this.l.DeleteLink(mockLink.Object);
        }

        /// <summary>
        /// Test not CRUD query
        /// </summary>
        [Test]
        public void GetCategoriesbyCountOfUsesTest()
        {
            List<CARMODEL> mockedModels = new List<CARMODEL>();
            List<EXTRA> mockedExtras = new List<EXTRA>();
            List<LINK_MOD_EXTRA> mockedLinks = new List<LINK_MOD_EXTRA>();

            mockedModels.Add(new CARMODEL { BRAND_ID = "a", PRICE = 100, ID = "x" });
            mockedModels.Add(new CARMODEL { BRAND_ID = "b", PRICE = 600, ID = "zz" });

            mockedExtras.Add(new EXTRA { ID = "a", CATEGORY = "asd", NAME = "cda" });
            mockedExtras.Add(new EXTRA { ID = "b", CATEGORY = "dsa", NAME = "acd" });
            mockedExtras.Add(new EXTRA { ID = "c", CATEGORY = "asd", NAME = "tga" });
            mockedExtras.Add(new EXTRA { ID = "d", CATEGORY = "dsa", NAME = "agt" });

            mockedLinks.Add(new LINK_MOD_EXTRA { ID = "0", MODEL_ID = "x", EXTRA_ID = "a" });
            mockedLinks.Add(new LINK_MOD_EXTRA { ID = "1", MODEL_ID = "x", EXTRA_ID = "b" });
            mockedLinks.Add(new LINK_MOD_EXTRA { ID = "2", MODEL_ID = "zz", EXTRA_ID = "c" });
            mockedLinks.Add(new LINK_MOD_EXTRA { ID = "3", MODEL_ID = "zz", EXTRA_ID = "d" });

            this.mockedRepo.Setup(repo => repo.GetAllModels()).Returns(mockedModels);
            this.mockedRepo.Setup(repo => repo.GetAllExtras()).Returns(mockedExtras);
            this.mockedRepo.Setup(repo => repo.GetAllLinks()).Returns(mockedLinks);

            this.l.GetCategoriesbyCountOfUses();
        }

        /// <summary>
        /// Test not CRUD query
        /// </summary>
        [Test]
        public void GetAveragePriceOfModelsByBrandTest()
        {
            List<CARMODEL> mockedModels = new List<CARMODEL>();
            List<CARBRAND> mockedBrands = new List<CARBRAND>();

            mockedModels.Add(new CARMODEL { BRAND_ID = "a", PRICE = 100 });
            mockedModels.Add(new CARMODEL { BRAND_ID = "a", PRICE = 300 });
            mockedModels.Add(new CARMODEL { BRAND_ID = "b", PRICE = 400 });
            mockedModels.Add(new CARMODEL { BRAND_ID = "b", PRICE = 600 });

            mockedBrands.Add(new CARBRAND { ID = "a", NAME = "A marka" });
            mockedBrands.Add(new CARBRAND { ID = "b", NAME = "B marka" });

            this.mockedRepo.Setup(repo => repo.GetAllModels()).Returns(mockedModels);
            this.mockedRepo.Setup(repo => repo.GetAllBrands()).Returns(mockedBrands);

            var x = this.l.GetAveragePriceOfModelsByBrand();
        }

        /// <summary>
        /// Test not CRUD query
        /// </summary>
        [Test]
        public void GetModelPricesWithExrasTest()
        {
            List<CARMODEL> mockedModels = new List<CARMODEL>();
            List<EXTRA> mockedExtras = new List<EXTRA>();
            List<LINK_MOD_EXTRA> mockedLinks = new List<LINK_MOD_EXTRA>();

            mockedModels.Add(new CARMODEL { BRAND_ID = "a", PRICE = 100, ID = "x" });
            mockedModels.Add(new CARMODEL { BRAND_ID = "b", PRICE = 600, ID = "zz" });

            mockedExtras.Add(new EXTRA { ID = "a", CATEGORY = "asd", NAME = "cda" });
            mockedExtras.Add(new EXTRA { ID = "b", CATEGORY = "dsa", NAME = "acd" });
            mockedExtras.Add(new EXTRA { ID = "c", CATEGORY = "asd", NAME = "tga" });
            mockedExtras.Add(new EXTRA { ID = "d", CATEGORY = "dsa", NAME = "agt" });

            mockedLinks.Add(new LINK_MOD_EXTRA { ID = "0", MODEL_ID = "x", EXTRA_ID = "a" });
            mockedLinks.Add(new LINK_MOD_EXTRA { ID = "1", MODEL_ID = "x", EXTRA_ID = "b" });
            mockedLinks.Add(new LINK_MOD_EXTRA { ID = "2", MODEL_ID = "zz", EXTRA_ID = "c" });
            mockedLinks.Add(new LINK_MOD_EXTRA { ID = "3", MODEL_ID = "zz", EXTRA_ID = "d" });

            this.mockedRepo.Setup(repo => repo.GetAllModels()).Returns(mockedModels);
            this.mockedRepo.Setup(repo => repo.GetAllExtras()).Returns(mockedExtras);
            this.mockedRepo.Setup(repo => repo.GetAllLinks()).Returns(mockedLinks);

            this.l.GetModelPricesWithExras();
        }
    }
}
