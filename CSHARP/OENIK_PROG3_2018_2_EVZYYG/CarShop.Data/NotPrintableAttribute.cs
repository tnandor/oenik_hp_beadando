﻿// <copyright file="NotPrintableAttribute.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Attribute to let the printing method know if the attribute is not directly printable
    /// </summary>
    public class NotPrintableAttribute : Attribute
    {
    }
}
