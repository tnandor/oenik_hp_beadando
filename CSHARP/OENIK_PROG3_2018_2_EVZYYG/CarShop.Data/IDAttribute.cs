﻿// <copyright file="IDAttribute.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Attribute to make difference between attributes and ID attributes
    /// </summary>
    public class IDAttribute : Attribute
    {
    }
}
