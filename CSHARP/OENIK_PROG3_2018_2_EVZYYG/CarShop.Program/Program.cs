﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Program
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CarShop.Data;
    using CarShop.Logic;
    using CarShop.Repository;

    /// <summary>
    /// This is the StartUp project
    /// </summary>
    internal class Program
    {
        private static void Main(string[] args)
        {
            Logic bL = new Logic(new DBRepository());

            bool esc = true;

            while (esc)
            {
            string option = UI.MainMenu();

            esc = bL.ChooseOption(option);

            Console.ReadKey();
            }
        }
    }
}
