﻿// <copyright file="UI.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Program
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Primitive User interface for the program
    /// </summary>
    public static class UI
    {
        /// <summary>
        /// That's the mainmenu
        /// </summary>
        /// <returns> The selected menu as a string </returns>
        public static string MainMenu()
        {
            Console.Clear();
            Console.WriteLine("Type the lower case letter of the action and the number of the table, you would like to work with!");
            Console.WriteLine("For example: l1 -- means List the entities of the Models table");
            Console.WriteLine("L - List entities");
            Console.WriteLine("C - Create and add an entitiy");
            Console.WriteLine("M - Modify an entity");
            Console.WriteLine("R - Remove an entity");
            Console.WriteLine();
            Console.WriteLine("1 - Models");
            Console.WriteLine("2 - Brands");
            Console.WriteLine("3 - Extras");
            Console.WriteLine("4 - Links between the tables");
            Console.WriteLine();
            Console.WriteLine("X - Something else:");
            Console.WriteLine("1 - Cars with prices of their extras");
            Console.WriteLine("2 - Average prices of cars of brands");
            Console.WriteLine("3 - Extra categories by how often ~attached~");
            Console.WriteLine("4 - A not CRUD query");
            Console.WriteLine("\nQ - Exit");
            return Console.ReadLine();
        }
    }
}
