﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CarShop.Data;

    /// <summary>
    /// This interface represents, what kind of methods should a Repository have
    /// </summary>
    public interface IRepository
    {
        /// <summary>
        /// This method returns all of the model enitites
        /// </summary>
        /// <returns> List with all of the model enitites </returns>
        List<CARMODEL> GetAllModels();

        /// <summary>
        /// This method returns all of the brand enitites
        /// </summary>
        /// <returns> List with all of the brand enitites </returns>
        List<CARBRAND> GetAllBrands();

        /// <summary>
        /// This method returns all of the extra enitites
        /// </summary>
        /// <returns> List with all of the extra enitites </returns>
        List<EXTRA> GetAllExtras();

        /// <summary>
        /// This method returns all of the link enitites
        /// </summary>
        /// <returns> List with all of the link enitites </returns>
        List<LINK_MOD_EXTRA> GetAllLinks();

        /// <summary>
        /// This method adds a model entity to the collection
        /// </summary>
        /// <param name="inp"> The model which the user would like to add </param>
        void AddModel(CARMODEL inp);

        /// <summary>
        /// This method adds a brand entity to the collection
        /// </summary>
        /// <param name="inp"> The brand which the user would like to add </param>
        void AddBrand(CARBRAND inp);

        /// <summary>
        /// This method adds an extra entity to the collection
        /// </summary>
        /// <param name="inp"> The extra which the user would like to add </param>
        void AddExtra(EXTRA inp);

        /// <summary>
        /// This method adds a link entity to the collection
        /// </summary>
        /// <param name="inp"> The link which the user would like to add </param>
        void AddLink(LINK_MOD_EXTRA inp);

        /// <summary>
        /// This method "overwrites" an existing item in the collection
        /// </summary>
        /// <param name="inp"> an entity with new properties </param>
        void UpdateModel(CARMODEL inp);

        /// <summary>
        /// This method "overwrites" an existing item in the collection
        /// </summary>
        /// <param name="inp"> an entity with new properties </param>
        void UpdateBrand(CARBRAND inp);

        /// <summary>
        /// This method "overwrites" an existing item in the collection
        /// </summary>
        /// <param name="inp"> an entity with new properties </param>
        void UpdateExtra(EXTRA inp);

        /// <summary>
        /// Deletes an item from the collection
        /// </summary>
        /// <param name="inp"> The item which the user wants to remove </param>
        void DeleteModel(CARMODEL inp);

        /// <summary>
        /// Deletes an item from the collection
        /// </summary>
        /// <param name="inp"> The item which the user wants to remove </param>
        void DeleteBrand(CARBRAND inp);

        /// <summary>
        /// Deletes an item from the collection
        /// </summary>
        /// <param name="inp"> The item which the user wants to remove </param>
        void DeleteExtra(EXTRA inp);

        /// <summary>
        /// Deletes an item from the collection
        /// </summary>
        /// <param name="inp"> The item which the user wants to remove </param>
        void DeleteLink(LINK_MOD_EXTRA inp);

        /// <summary>
        /// thingy
        /// </summary>
        /// <param name="idToFind"> parameter </param>
        /// <returns> a thing </returns>
        CARMODEL FindModel(string idToFind);

        /// <summary>
        /// thingy
        /// </summary>
        /// <param name="idToFind"> parameter </param>
        /// <returns> a thing </returns>
        CARBRAND FindBrand(string idToFind);

        /// <summary>
        /// thingy
        /// </summary>
        /// <param name="idToFind"> parameter </param>
        /// <returns> a thing </returns>
        EXTRA FindExtra(string idToFind);

        /// <summary>
        /// thingy
        /// </summary>
        /// <param name="idToFind"> parameter </param>
        /// <returns> a thing </returns>
        LINK_MOD_EXTRA FindLink(string idToFind);
    }
}
