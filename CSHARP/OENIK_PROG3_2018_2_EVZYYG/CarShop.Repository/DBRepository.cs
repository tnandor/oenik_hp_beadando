﻿// <copyright file="DBRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Threading.Tasks;
    using CarShop.Data;

    /// <summary>
    /// This is my repository for the database operations
    /// </summary>
    public class DBRepository : IRepository
    {
        /// <summary>
        /// The repository have to have a database to work with
        /// </summary>
        private CarShopDataEntities dataBase;

        /// <summary>
        /// Initializes a new instance of the <see cref="DBRepository"/> class.
        /// </summary>
        public DBRepository()
        {
            this.dataBase = new CarShopDataEntities();
        }

        /// <summary>
        /// Adds the input record to the table
        /// </summary>
        /// <param name="inp"> The record which the user like to add to the table </param>
        public void AddBrand(CARBRAND inp)
        {
            this.dataBase.CARBRANDS.Add(inp);
            this.dataBase.SaveChanges();
        }

        /// <summary>
        /// Adds the input record to the table
        /// </summary>
        /// <param name="inp"> The record which the user like to add to the table </param>
        public void AddExtra(EXTRA inp)
        {
            this.dataBase.EXTRAS.Add(inp);
            this.dataBase.SaveChanges();
        }

        /// <summary>
        /// Adds the input record to the table
        /// </summary>
        /// <param name="inp"> The record which the user like to add to the table </param>
        public void AddLink(LINK_MOD_EXTRA inp)
        {
            this.dataBase.LINK_MOD_EXTRA.Add(inp);
            this.dataBase.SaveChanges();
        }

        /// <summary>
        /// Adds the input record to the table
        /// </summary>
        /// <param name="inp"> The record which the user like to add to the table </param>
        public void AddModel(CARMODEL inp)
        {
            this.dataBase.CARMODELS.Add(inp);

            // this.dataBase.Entry(inp).State = System.Data.Entity.EntityState.Added;
            this.dataBase.SaveChanges();
        }

        /// <summary>
        /// Deletes the input parameter from the table
        /// </summary>
        /// <param name="inp"> The record which the user like to remove </param>
        public void DeleteBrand(CARBRAND inp)
        {
            this.dataBase.CARBRANDS.Remove(inp);
            this.dataBase.SaveChanges();
        }

        /// <summary>
        /// Deletes the input parameter from the table
        /// </summary>
        /// <param name="inp"> The record which the user like to remove </param>
        public void DeleteExtra(EXTRA inp)
        {
            this.dataBase.EXTRAS.Remove(inp);
            this.dataBase.SaveChanges();
        }

        /// <summary>
        /// Deletes the input parameter from the table
        /// </summary>
        /// <param name="inp"> The record which the user like to remove </param>
        public void DeleteLink(LINK_MOD_EXTRA inp)
        {
            this.dataBase.LINK_MOD_EXTRA.Remove(inp);
            this.dataBase.SaveChanges();
        }

        /// <summary>
        /// Deletes the input parameter from the table
        /// </summary>
        /// <param name="inp"> The record which the user like to remove </param>
        public void DeleteModel(CARMODEL inp)
        {
            this.dataBase.CARMODELS.Remove(inp);
            this.dataBase.SaveChanges();
        }

        /// <summary>
        /// Finds a model in the model table
        /// </summary>
        /// <returns> A model record </returns>
        /// <param name="idToFind"> The ID of the record which the user like to work with </param>
        public CARMODEL FindModel(string idToFind)
        {
            return this.dataBase.CARMODELS.FirstOrDefault(a => a.ID == idToFind);
        }

        /// <summary>
        /// Finds a brand in the brand table
        /// </summary>
        /// <returns> A brand record </returns>
        /// <param name="idToFind"> The ID of the record which the user like to work with </param>
        public CARBRAND FindBrand(string idToFind)
        {
            return this.dataBase.CARBRANDS.FirstOrDefault(a => a.ID == idToFind);
        }

        /// <summary>
        /// Finds an extra in the extra table
        /// </summary>
        /// <returns> An extra record </returns>
        /// <param name="idToFind"> The ID of the record which the user like to work with </param>
        public EXTRA FindExtra(string idToFind)
        {
            return this.dataBase.EXTRAS.FirstOrDefault(a => a.ID == idToFind);
        }

        /// <summary>
        /// Finds a link in the link table
        /// </summary>
        /// <returns> A link record </returns>
        /// <param name="idToFind"> The ID of the record which the user like to work with </param>
        public LINK_MOD_EXTRA FindLink(string idToFind)
        {
            return this.dataBase.LINK_MOD_EXTRA.FirstOrDefault(a => a.ID == idToFind);
        }

        /// <summary>
        /// Returns all of the brand records
        /// </summary>
        /// <returns> List of the brands </returns>
        public List<CARBRAND> GetAllBrands()
        {
            var brands = this.dataBase.CARBRANDS;
            return brands.ToList();
        }

        /// <summary>
        /// Returns all of the extra records
        /// </summary>
        /// <returns> List of the extras </returns>
        public List<EXTRA> GetAllExtras()
        {
            var extras = this.dataBase.EXTRAS;
            return extras.ToList();
        }

        /// <summary>
        /// Returns all of the link records
        /// </summary>
        /// <returns> List of the links </returns>
        public List<LINK_MOD_EXTRA> GetAllLinks()
        {
            var links = this.dataBase.LINK_MOD_EXTRA;
            return links.ToList();
        }

        /// <summary>
        /// Returns all of the model records
        /// </summary>
        /// <returns> List of the models </returns>
        public List<CARMODEL> GetAllModels()
        {
            var models = this.dataBase.CARMODELS;
            return models.ToList();
        }

        /// <summary>
        /// Updates a brand
        /// </summary>
        /// <param name="inp"> input parameter </param>
        public void UpdateBrand(CARBRAND inp)
        {
            if (inp != null)
            {
                var old = this.dataBase.CARBRANDS.SingleOrDefault(a => a.ID == inp.ID);

                foreach (PropertyInfo newProp in inp.GetType().GetProperties())
                {
                    if (newProp.GetValue(inp) != null && newProp.GetValue(inp) != newProp.GetValue(old))
                    {
                        newProp.SetValue(old, newProp.GetValue(inp));
                    }
                }

                this.dataBase.SaveChanges();
            }
            else
            {
                throw new NullReferenceException();
            }
        }

        /// <summary>
        /// Updates an extra
        /// </summary>
        /// <param name="inp"> input parameter </param>
        public void UpdateExtra(EXTRA inp)
        {
            if (inp != null)
            {
                var old = this.dataBase.EXTRAS.SingleOrDefault(a => a.ID == inp.ID);

                foreach (PropertyInfo newProp in inp.GetType().GetProperties())
                {
                    if (newProp.GetValue(inp) != null && newProp.GetValue(inp) != newProp.GetValue(old))
                    {
                        newProp.SetValue(old, newProp.GetValue(inp));
                    }
                }

                this.dataBase.SaveChanges();
            }
            else
            {
                throw new NullReferenceException();
            }
        }

        /// <summary>
        /// Updates a model
        /// </summary>
        /// <param name="inp"> input parameter </param>
        public void UpdateModel(CARMODEL inp)
        {
            if (inp != null)
            {
                var old = this.dataBase.CARMODELS.SingleOrDefault(a => a.ID == inp.ID);

                foreach (PropertyInfo newProp in inp.GetType().GetProperties())
                {
                    if (newProp.GetValue(inp) != null && newProp.GetValue(inp) != newProp.GetValue(old))
                    {
                        newProp.SetValue(old, newProp.GetValue(inp));
                    }
                }

                this.dataBase.SaveChanges();
            }
            else
            {
                throw new NullReferenceException();
            }
        }
    }
}
