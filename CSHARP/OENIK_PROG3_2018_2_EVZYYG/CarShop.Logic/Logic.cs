﻿// <copyright file="Logic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Threading.Tasks;
    using CarShop.Data;
    using CarShop.Repository;

    /// <summary>
    /// thingy
    /// </summary>
    public class Logic : ILogic
    {
        private readonly IRepository repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="Logic"/> class.
        /// </summary>
        /// <param name="repo"> repository parameter </param>
        public Logic(IRepository repo) => this.repo = repo;

        /// <summary>
        /// This method read what the user wants to do
        /// </summary>
        /// <returns> if user wants to !exit </returns>
        /// <param name="option"> repository parameter </param>
        public bool ChooseOption(string option)
        {
            if (option == string.Empty)
            {
                return true;
            }

            Console.Clear();
            switch (option[0])
            {
                case 'l':
                    switch (option[1])
                    {
                        case '1':
                            // List models
                            var q0 = this.GetAllModels();
                            this.PrintList(q0.AsQueryable());
                            break;
                        case '2':
                            // List brands
                            var q1 = this.GetAllBrands();
                            this.PrintList(q1.AsQueryable());
                            break;
                        case '3':
                            // List extras
                            var q2 = this.GetAllExtras();
                            this.PrintList(q2.AsQueryable());
                            break;
                        case '4':
                            // List links
                            var q3 = this.GetAllLinks();
                            this.PrintList(q3.AsQueryable());
                            break;
                        default:
                            break;
                    }

                    break;
                case 'c':
                    switch (option[1])
                    {
                        case '1':
                            //// Add new model
                            object c0 = new CARMODEL();
                            this.CreateEntity(ref c0);
                            this.AddModel((CARMODEL)c0);
                            break;
                        case '2':
                            // Add new brand
                            object c1 = new CARBRAND();
                            this.CreateEntity(ref c1);
                            this.AddBrand((CARBRAND)c1);
                            break;
                        case '3':
                            // Add new extra
                            object c2 = new EXTRA();
                            this.CreateEntity(ref c2);
                            this.AddExtra((EXTRA)c2);
                            break;
                        case '4':
                            // Add new link
                            object c3 = new LINK_MOD_EXTRA();
                            this.CreateEntity(ref c3);
                            this.AddLink((LINK_MOD_EXTRA)c3);
                            break;
                        default:
                            break;
                    }

                    break;
                case 'm':
                    switch (option[1])
                    {
                        case '1':
                            // Modify model
                            CARMODEL temp0 = this.FindModifyModel();
                            if (temp0 != null)
                            {
                                this.UpdateModel(temp0);
                            }

                            break;
                        case '2':
                            // Modify brand
                            CARBRAND temp1 = this.FindModifyBrand();
                            if (temp1 != null)
                            {
                                this.UpdateBrand(temp1);
                            }

                            break;
                        case '3':
                            // Modify extra
                            EXTRA temp2 = this.FindModifyExtra();
                            if (temp2 != null)
                            {
                                this.UpdateExtra(temp2);
                            }

                            break;
                        case '4':
                            Console.WriteLine("You cannot modify a link :(");
                            break;
                        default:
                            break;
                    }

                    break;
                case 'r':
                    switch (option[1])
                    {
                        case '1':
                            // Remove model
                            CARMODEL temp0 = this.FindModelToDelete();
                            if (temp0 != null)
                            {
                                try
                                {
                                    this.DeleteModel(temp0);
                                }
                                catch (System.Data.Entity.Infrastructure.DbUpdateException)
                                {
                                    Console.WriteLine("Error: Other entities are depending on this element");
                                }
                            }

                            break;
                        case '2':
                            // Remove brand
                            CARBRAND temp1 = this.FindBrandToDelete();
                            if (temp1 != null)
                            {
                                try
                                {
                                    this.DeleteBrand(temp1);
                                }
                                catch (System.Data.Entity.Infrastructure.DbUpdateException)
                                {
                                    Console.WriteLine("Error: Other entities are depending on this element");
                                }
                            }

                            break;
                        case '3':
                            // Remove extra
                            EXTRA temp2 = this.FindExtraToDelete();
                            if (temp2 != null)
                            {
                                try
                                {
                                    this.DeleteExtra(temp2);
                                }
                                catch (System.Data.Entity.Infrastructure.DbUpdateException)
                                {
                                    Console.WriteLine("Error: Other entities are depending on this element");
                                }
                            }

                            break;
                        case '4':
                            // Remove link
                            LINK_MOD_EXTRA temp3 = this.FindLinkToDelete();
                            if (temp3 != null)
                            {
                                try
                                {
                                    this.DeleteLink(temp3);
                                }
                                catch (System.Data.Entity.Infrastructure.DbUpdateException)
                                {
                                    Console.WriteLine("Error: Other entities are depending on this element");
                                }
                            }

                            break;
                        default:
                            break;
                    }

                    break;
                case 'x':

                    switch (option[1])
                    {
                        case '1':
                            this.PrintList(this.GetModelPricesWithExras());
                            break;
                        case '2':
                            // Average price of cars by brands
                            this.PrintList(this.GetAveragePriceOfModelsByBrand());
                            break;
                        case '3':
                            this.PrintList(this.GetCategoriesbyCountOfUses());
                            break;
                        case '4':
                            Console.WriteLine("Nothing exciting here");
                            break;
                        default:
                            break;
                    }

                    break;
                case 'q':
                    Console.WriteLine("Have a nice day!");
                    return false;
                default:
                    break;
            }

            return true;
        }

        /// <summary>
        /// The third not CRUD method
        /// </summary>
        /// <returns> A collection of categories by count of uses </returns>
        public IQueryable GetCategoriesbyCountOfUses()
        {
            var allModels = this.GetAllModels();
            var allExtras = this.GetAllExtras();
            var allLinks = this.GetAllLinks();

            var linksByExtras2 = from x in allLinks
                                 join y in allExtras on x.EXTRA_ID equals y.ID
                                 group x by x.EXTRA_ID into g
                                 select new
                                 {
                                     g.Key,
                                     CountOfUses = g.Count()
                                 };

            var categoriesAndCountOfUses = from x in allExtras
                                           join y in linksByExtras2 on x.ID equals y.Key
                                           select new
                                           {
                                               Name = x.CATEGORY,
                                               y.CountOfUses
                                           };
            var categoriesbyCountOfUses = from x in categoriesAndCountOfUses
                                          group x by x.Name into g
                                          select new
                                          {
                                              Name = g.Key,
                                              Sum = g.Sum(a => a.CountOfUses)
                                          };

            return categoriesbyCountOfUses.AsQueryable();
        }

        /// <summary>
        /// The second not CRUD method
        /// </summary>
        /// <returns> A collection of brands by average price of their models</returns>
        public IQueryable GetAveragePriceOfModelsByBrand()
        {
            var allModels = this.GetAllModels();
            var allBrands = this.GetAllBrands();

            var modelsByID = from x in allModels
                             group x by x.BRAND_ID into g
                             select new
                             {
                                 BrandID = g.Key,
                                 AvgPrice = g.Average(a => a.PRICE)
                             };

            var joinBrandsAndPrice = from x in allBrands
                                     join y in modelsByID on x.ID equals y.BrandID
                                     select new
                                     {
                                         Name = x.NAME,
                                         y.AvgPrice
                                     };

            return joinBrandsAndPrice.AsQueryable();
        }

        /// <summary>
        /// The first not CRUD method
        /// </summary>
        /// <returns> A collection of models by their prices with extras </returns>
        public IQueryable GetModelPricesWithExras()
        {
            var allModels = this.GetAllModels();
            var allExtras = this.GetAllExtras();
            var allLinks = this.GetAllLinks();

            var linksByExtras = from x in allLinks
                                join y in allExtras on x.EXTRA_ID equals y.ID
                                group x by x.MODEL_ID into g
                                select new
                                {
                                    ModelID = g.Key,
                                    Sum = g.Sum(a => a.EXTRA.PRICE)
                                };

            var joinModelPrices = from x in allModels
                                  join y in linksByExtras on x.ID equals y.ModelID
                                  select new
                                  {
                                      x.NAME,
                                      PriceWithExtras = y.Sum + x.PRICE,
                                      y.Sum
                                  };

            return joinModelPrices.AsQueryable();
        }

        // private LINK_MOD_EXTRA CreateLink()
        // {
        //    throw new NotImplementedException();
        // }

        // private EXTRA CreateExtra()
        // {
        //    throw new NotImplementedException();
        // }

        // private CARBRAND CreateBrand()
        // {
        //    throw new NotImplementedException();
        // }

        // private CARMODEL CreateModel()
        // {
        //    throw new NotImplementedException();
        // }

        /// <summary>
        /// This method prints the acquired queries
        /// </summary>
        /// <param name="inp"> collection of items from a query </param>
        public void PrintList(IQueryable inp)
        {
            int x = 0;

            foreach (var item in inp)
            {
                if (x < 1)
                {
                    foreach (PropertyInfo prop in item.GetType().GetProperties())
                    {
                        if (prop.GetCustomAttribute<NotPrintableAttribute>() == null)
                        {
                            Console.Write(prop.Name + "\t");
                        }
                    }
                }

                this.PrintItem(item);
                x++;
            }

            Console.WriteLine("\nPrinted " + x + " items");
        }

        /// <summary>
        /// This method adds a model entity to the collection
        /// </summary>
        /// <param name="inp"> The model which the user would like to add </param>
        public void AddModel(CARMODEL inp)
        {
            this.repo.AddModel(inp);
        }

        /// <summary>
        /// This method adds a brand entity to the collection
        /// </summary>
        /// <param name="inp"> The brand which the user would like to add </param>
        public void AddBrand(CARBRAND inp)
        {
            this.repo.AddBrand(inp);
        }

        /// <summary>
        /// This method adds an extra entity to the collection
        /// </summary>
        /// <param name="inp"> The extra which the user would like to add </param>
        public void AddExtra(EXTRA inp)
        {
            this.repo.AddExtra(inp);
        }

        /// <summary>
        /// This method adds a link entity to the collection
        /// </summary>
        /// <param name="inp"> The link which the user would like to add </param>
        public void AddLink(LINK_MOD_EXTRA inp)
        {
            this.repo.AddLink(inp);
        }

        /// <summary>
        /// This method returns all of the model enitites
        /// </summary>
        /// <returns> List with all of the model enitites </returns>
        public List<CARMODEL> GetAllModels()
        {
            return this.repo.GetAllModels();
        }

        /// <summary>
        /// This method returns all of the brand enitites
        /// </summary>
        /// <returns> List with all of the brand enitites </returns>
        public List<CARBRAND> GetAllBrands()
        {
            return this.repo.GetAllBrands();
        }

        /// <summary>
        /// This method returns all of the extra enitites
        /// </summary>
        /// <returns> List with all of the extra enitites </returns>
        public List<EXTRA> GetAllExtras()
        {
            return this.repo.GetAllExtras();
        }

        /// <summary>
        /// This method returns all of the link enitites
        /// </summary>
        /// <returns> List with all of the link enitites </returns>
        public List<LINK_MOD_EXTRA> GetAllLinks()
        {
            return this.repo.GetAllLinks();
        }

        /// <summary>
        /// Finds an entity to modify then returns it
        /// </summary>
        /// <returns> The found entity </returns>
        public CARMODEL FindModifyModel()
        {
            Console.WriteLine("Type the ID of the model!");
            string s = Console.ReadLine();
            object temp = this.repo.FindModel(s);
            if (temp != null)
            {
                Console.WriteLine("Item found: ");
                this.PrintItemWInfo(temp);
                Console.WriteLine("\n\nEnter a new value if you'd like to modify an attribute!");
                this.CreateEntity(ref temp, this.HaveCostumAttribute, typeof(IDAttribute));
                return (CARMODEL)temp;
            }
            else
            {
                Console.WriteLine("Sorry, there isn't any entity with this ID");
                Console.ReadKey();
                return null;
            }
        }

        /// <summary>
        /// Finds an entity to modify then returns it
        /// </summary>
        /// <returns> The found entity </returns>
        public CARBRAND FindModifyBrand()
        {
            Console.WriteLine("Type the ID of the Brand!");
            string s = Console.ReadLine();
            object temp = this.repo.FindBrand(s);
            if (temp != null)
            {
                Console.WriteLine("Item found: ");
                this.PrintItemWInfo(temp);
                Console.WriteLine("\n\nEnter a new value if you'd like to modify an attribute!");
                this.CreateEntity(ref temp, this.HaveCostumAttribute, typeof(IDAttribute));
                return (CARBRAND)temp;
            }
            else
            {
                Console.WriteLine("Sorry, there isn't any entity with this ID");
                Console.ReadKey();
                return null;
            }
        }

        /// <summary>
        /// Finds an entity to modify then returns it
        /// </summary>
        /// <returns> The found entity </returns>
        public EXTRA FindModifyExtra()
        {
            Console.WriteLine("Type the ID of the model!");
            string s = Console.ReadLine();
            object temp = this.repo.FindExtra(s);
            if (temp != null)
            {
                Console.WriteLine("Item found: ");
                this.PrintItemWInfo(temp);
                Console.WriteLine("\n\nEnter a new value if you'd like to modify an attribute!");
                this.CreateEntity(ref temp, this.HaveCostumAttribute, typeof(IDAttribute));
                return (EXTRA)temp;
            }
            else
            {
                Console.WriteLine("Sorry, there isn't any entity with this ID");
                Console.ReadKey();
                return null;
            }
        }

        /// <summary>
        /// This method "overwrites" an existing item in the collection
        /// </summary>
        /// <param name="inp"> an entity with new properties </param>
        public void UpdateModel(CARMODEL inp)
        {
            this.repo.UpdateModel(inp);
        }

        /// <summary>
        /// This method "overwrites" an existing item in the collection
        /// </summary>
        /// <param name="inp"> an entity with new properties </param>
        public void UpdateBrand(CARBRAND inp)
        {
            this.repo.UpdateBrand(inp);
        }

        /// <summary>
        /// This method "overwrites" an existing item in the collection
        /// </summary>
        /// <param name="inp"> an entity with new properties </param>
        public void UpdateExtra(EXTRA inp)
        {
            this.repo.UpdateExtra(inp);
        }

        /// <summary>
        /// Finds an entity to remove then returns it
        /// </summary>
        /// <returns> The found entity </returns>
        public CARMODEL FindModelToDelete()
        {
            Console.WriteLine("Type the ID of the model!");
            string s = Console.ReadLine();
            CARMODEL temp = this.repo.FindModel(s);
            if (temp != null)
            {
                Console.WriteLine("Type again to confirm!");
                if (s == Console.ReadLine())
                {
                    return temp;
                }
            }
            else
            {
                Console.WriteLine("Sorry, there isn't any entity with this ID");
                Console.ReadKey();
            }

            return null;
        }

        /// <summary>
        /// Finds an entity to remove then returns it
        /// </summary>
        /// <returns> The found entity </returns>
        public CARBRAND FindBrandToDelete()
        {
            Console.WriteLine("Type the ID of the Brand!");
            string s = Console.ReadLine();
            CARBRAND temp = this.repo.FindBrand(s);
            if (temp != null)
            {
                Console.WriteLine("Type again to confirm!");
                if (s == Console.ReadLine())
                {
                    return temp;
                }
            }
            else
            {
                Console.WriteLine("Sorry, there isn't any entity with this ID");
                Console.ReadKey();
            }

            return null;
        }

        /// <summary>
        /// Finds an entity to remove then returns it
        /// </summary>
        /// <returns> The found entity </returns>
        public EXTRA FindExtraToDelete()
        {
            Console.WriteLine("Type the ID of the Extra!");
            string s = Console.ReadLine();
            EXTRA temp = this.repo.FindExtra(s);
            if (temp != null)
            {
                Console.WriteLine("Type again to confirm!");
                if (s == Console.ReadLine())
                {
                    return temp;
                }
            }
            else
            {
                Console.WriteLine("Sorry, there isn't any entity with this ID");
                Console.ReadKey();
            }

            return null;
        }

        /// <summary>
        /// Finds an entity to remove then returns it
        /// </summary>
        /// <returns> The found entity </returns>
        public LINK_MOD_EXTRA FindLinkToDelete()
        {
            Console.WriteLine("Type the ID of the Link!");
            string s = Console.ReadLine();
            LINK_MOD_EXTRA temp = this.repo.FindLink(s);
            if (temp != null)
            {
                Console.WriteLine("Type again to confirm!");
                if (s == Console.ReadLine())
                {
                    return temp;
                }
            }
            else
            {
                Console.WriteLine("Sorry, there isn't any entity with this ID");
                Console.ReadKey();
            }

            return null;
        }

        /// <summary>
        /// Deletes an item from the collection
        /// </summary>
        /// <param name="inp"> The item which the user wants to remove </param>
        public void DeleteModel(CARMODEL inp)
        {
            this.repo.DeleteModel(inp);
        }

        /// <summary>
        /// Deletes an item from the collection
        /// </summary>
        /// <param name="inp"> The item which the user wants to remove </param>
        public void DeleteBrand(CARBRAND inp)
        {
            this.repo.DeleteBrand(inp);
        }

        /// <summary>
        /// Deletes an item from the collection
        /// </summary>
        /// <param name="inp"> The item which the user wants to remove </param>
        public void DeleteExtra(EXTRA inp)
        {
            this.repo.DeleteExtra(inp);
        }

        /// <summary>
        /// Deletes an item from the collection
        /// </summary>
        /// <param name="inp"> The item which the user wants to remove </param>
        public void DeleteLink(LINK_MOD_EXTRA inp)
        {
            this.repo.DeleteLink(inp);
        }

        private void BreakDependenciesModel(CARMODEL inp)
        {
            var link = this.repo.FindLink(inp.ID);

            if (link != null)
            {
                this.repo.DeleteLink(link);
            }
        }

        private void CreateEntity(ref object inp)
        {
            PropertyInfo[] props = inp.GetType().GetProperties();
            Console.WriteLine("Make sure you type correct data!");
            foreach (PropertyInfo item in props)
            {
                if (item.GetCustomAttribute<NotPrintableAttribute>() == null)
                {
                    Console.WriteLine(item.Name + " [" + item.PropertyType.Name + "] ");
                    string x = Console.ReadLine();
                    if (x == string.Empty)
                    {
                    }
                    else if (item.PropertyType == typeof(decimal?))
                    {
                        item.SetValue(inp, decimal.Parse(x));
                    }
                    else if (item.PropertyType == typeof(DateTime?))
                    {
                        if (DateTime.TryParse(x, out DateTime userDateTime))
                        {
                            item.SetValue(inp, userDateTime);
                        }
                    }
                    else if (item.PropertyType == typeof(bool?))
                    {
                        item.SetValue(inp, bool.Parse(x));
                    }
                    else
                    {
                        /*if(item.PropertyType == typeof(string))*/
                        item.SetValue(inp, x);
                    }
                }
            }
        }

        private void CreateEntity(ref object inp, Func<PropertyInfo, Type, bool> attr, Type cattrType)
        {
            PropertyInfo[] props = inp.GetType().GetProperties();
            Console.WriteLine("Make sure you type correct data!");
            foreach (PropertyInfo item in props)
            {
                if (attr(item, cattrType))
                {
                }
                else if (item.GetCustomAttribute<NotPrintableAttribute>() == null)
                {
                    Console.WriteLine(item.Name + " [" + item.PropertyType.Name + "] ");
                    string x = Console.ReadLine();

                    if (x == string.Empty)
                    {
                    }
                    else if (item.PropertyType == typeof(decimal?))
                    {
                        item.SetValue(inp, decimal.Parse(x));
                    }
                    else if (item.PropertyType == typeof(DateTime?))
                    {
                        if (DateTime.TryParse(x, out DateTime userDateTime))
                        {
                            item.SetValue(inp, userDateTime);
                        }
                    }
                    else if (item.PropertyType == typeof(bool?))
                    {
                        item.SetValue(inp, bool.Parse(x));
                    }
                    else
                    {
                        /*if(item.PropertyType == typeof(string))*/
                        item.SetValue(inp, x);
                    }
                }
            }
        }

        private bool HaveCostumAttribute(PropertyInfo info, Type costumAttr)
        {
            return info.GetCustomAttribute(costumAttr) != null;
        }

        private void PrintItem(object inp)
        {
            PropertyInfo[] props = inp.GetType().GetProperties();

            Console.WriteLine();

            foreach (PropertyInfo prop in props)
            {
                if (prop.GetCustomAttribute<NotPrintableAttribute>() == null)
                {
                    Console.Write(prop.GetValue(inp) + "\t");
                }
            }
        }

        private void PrintItemWInfo(object inp)
        {
            PropertyInfo[] props = inp.GetType().GetProperties();

            Console.WriteLine();

            foreach (PropertyInfo prop in props)
            {
                if (prop.GetCustomAttribute<NotPrintableAttribute>() == null)
                {
                    Console.Write(prop.Name + "\t");
                }
            }

            Console.WriteLine();
            foreach (PropertyInfo prop in props)
            {
                if (prop.GetCustomAttribute<NotPrintableAttribute>() == null)
                {
                    Console.Write(prop.GetValue(inp) + "\t");
                }
            }
        }
    }
}
